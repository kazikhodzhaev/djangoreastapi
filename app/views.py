# view.py виюшка пиложение для получение и обновление данных
# импорт из самого Django
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404
from django.db import transaction
# импорт моделей django
from .models import Page
# импортируем serializer
from .serializers import PagesSerializer, PageContentSerializer
# импорт из rest_framework
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.pagination import PageNumberPagination
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
# импортирование функцию отвечающий за обновление данных в фоновом режиме
from .tasks import update_count
# Create your views here.


# Получение списка страниц наследует class ListAPIView из rest fraemwork
class Pages(generics.ListAPIView):
	queryset = Page.objects.all()
	serializer_class = PagesSerializer
	pagination_class = PageNumberPagination


# Получение детальной инфомации о странице и вложенных данных наследует class APIView из rest fraemwork
class PageContent(APIView):

    # функция get которое работает атомарно и сразу исползует функцию созданную с 
    # помощю Celery для обновление данных в фоновом режиме
    @transaction.atomic
    def get(self, request, pk, format=None):
        page_detail = update_count(pk)
        serializer = PageContentSerializer(page_detail)
        return Response(serializer.data)

