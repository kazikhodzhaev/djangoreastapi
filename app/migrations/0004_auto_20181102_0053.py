from django.db import migrations
from datetime import date, datetime


def forwards_func(apps, schema_editor):
    # Миграция для заполнение базы данных перво начальными данными
    Page = apps.get_model("app", "Page")
    Content = apps.get_model("app", "Content")
    AudioContent = apps.get_model("app", "AudioContent")
    VideoContent = apps.get_model("app", "VideoContent")
    db_alias = schema_editor.connection.alias
    Page.objects.using(db_alias).bulk_create([
        Page(id=1, title="Футбол"),
        Page(id=2, title="Солничная система"),
        Page(id=3, title="BMW"),
    ])
    Content.objects.using(db_alias).bulk_create([
        Content(page=Page.objects.get(id=1), title="Новости Футбола", text="Во втоник играли Португалия и Германия, счет был 4-4 было нечия.", count=0),
        Content(page=Page.objects.get(id=1) ,title="Новости Футбола", text="Месси стал обладателям еще одного золотого мича!", count=0),
        Content(page=Page.objects.get(id=2) ,title="Компания Sollory построил саммую больщую систему", text="В северной Австралии компании Солари во главе Илона Маска построили сольнечную систему", count=0),
        Content(page=Page.objects.get(id=3), title="BMW X7", text="BMW скором времени приступить к выпуску серии X7", count=0),
    ])
    AudioContent.objects.using(db_alias).bulk_create([
        AudioContent(page=Page.objects.get(id=1) ,title="Песня для поклоников Футбола", file="audio/futbol-bol-she-chem-prostaya-igra--futbol.mp3", count=0),
        AudioContent(page=Page.objects.get(id=3) ,title="Звук мотора BMW", file="audio/zvuk-motora-----bmv-e-46-2-5l-192-l-s.mp3", count=0),
        AudioContent(page=Page.objects.get(id=1) ,title="Песня", file="audio/futbol-muzyka-ligi-chempionov.mp3", count=0),
    ])
    VideoContent.objects.using(db_alias).bulk_create([
    	VideoContent(page=Page.objects.get(id=1) ,title="Самые крутые голы", file="video/640x352_dynamo_kiev_manchester_city_1_3.mp4 ", count=0),
    	VideoContent(page=Page.objects.get(id=1) ,title="Месси тактика игры", file="video/640x352_arsenal_barcelona_0_1.mp4", count=0),
    	VideoContent(page=Page.objects.get(id=2) ,title="Видео солнечная система", file="video/videoplayback.mp4", count=0),
    	VideoContent(page=Page.objects.get(id=3) ,title="BMW M5 e36", file="video/[v-s.mobi]BMWE34.3gp", count=0),
    	VideoContent(page=Page.objects.get(id=3) ,title="BMW M3", file="video/[v-s.mobi]DriFtBMWM3.mp4", count=0),
    ])


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20181101_2159'),
    ]

    operations = [
    	migrations.RunPython(forwards_func)
    ]
