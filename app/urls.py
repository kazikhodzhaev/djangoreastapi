# api aplication urls
from django.contrib import admin
from django.urls import path
from app import views


# url страницы приложение для получение данных
urlpatterns = [
    path('page/', views.Pages.as_view()),
    path('page/<int:pk>', views.PageContent.as_view(), name='page-detail'),
]
