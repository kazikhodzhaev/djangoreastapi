# serializer для получение и переоброзование данных для передачи в виде ответа 
from rest_framework import serializers
from .models import Page


# Serializer для получение списка страниц и сылки для подробной информации о странице
class PagesSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Page
		fields = ('url', 'id', 'title', 'date_created')


# Serilizer для получение подробноф информации о странице и все то что связано с страницей
class PageContentSerializer(serializers.ModelSerializer):

	class Meta:
		model = Page
		fields = ('title', 'date_created', 'text_content', 'audio_content', 'video_content')
		depth = 1

