# импорт logging для отоброжение ошибок в логе
import logging
# import celery app from root project
from testwork.celeryapp import app
from .models import Page, Content, AudioContent, VideoContent
from django.db.models import F
 
 
# таск которое исползуется celery для обновление данных в фоновом режиме
@app.task
def update_count(pk):
    try:
    	# обновление и получение данных
	    Content.objects.filter(page__pk=pk).update(count=F('count')+1)
	    AudioContent.objects.filter(page__pk=pk).update(count=F('count')+1)
	    VideoContent.objects.filter(page__pk=pk).update(count=F('count')+1)
	    return Page.objects.get(pk=pk)
    except:
    	# ошибка которое при возникновение обновление и получение срабатывает
        logging.warning("Update count objects in page wrong pleas check the object, be shure that tha all think is good!")