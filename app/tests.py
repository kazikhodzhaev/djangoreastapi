# Тесты для тестироваие приложение получение данных
# Используется класс rest wramework
from rest_framework.test import RequestsClient
# Create your tests here.


client = RequestsClient()

# Тестирование получение списка страниц
response = client.get('http://0.0.0.0:8000/api/v1/page')
assert response.status_code == 200
print('Get pages -> Ok')


# Тестирование получение детальной информации о странице
response = client.get('http://0.0.0.0:8000/api/v1/page/1')
assert response.status_code == 200
print('Get page content -> Ok')


