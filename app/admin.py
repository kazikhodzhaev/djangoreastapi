from django.contrib import admin
from django.contrib.admin import TabularInline, ModelAdmin
from .models import Page, Content, AudioContent, VideoContent
# Register your models here.


# создание классов для добовление данных через Page(страницы)
class ContentInline(TabularInline):
    model = Content
    insert_after = 'title'
    extra = 1

    def get_extra (self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra


class AudioContentInline(TabularInline):
    model = AudioContent
    insert_after = 'title'
    extra = 1

    def get_extra (self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra


class VideoContentInline(TabularInline):
    model = VideoContent
    insert_after = 'title'
    extra = 1

    def get_extra (self, request, obj=None, **kwargs):
        if obj:
            return 1
        return self.extra


# класс отвечающий за Page(страница) и которое имейт в нутри себя подключенные контенты других моделей для удобство работы
class PageAdmin(ModelAdmin):
    # Поиск по титулу
	search_fields = ('title', )
    # inline которое включает в себе другие моделии
	inlines = [
		ContentInline,
		AudioContentInline,
		VideoContentInline
	]


# админ тескстового контента
class ContentAdmin(ModelAdmin):
	search_fields = ('title', )


# админ аудио контента
class AudioContentAdmin(ModelAdmin):
	search_fields = ('title', )


# админ видео контента
class VideoContentAdmin(ModelAdmin):
	search_fields = ('title', )


# регистрация моделей в алминке
admin.site.register(Page, PageAdmin)
admin.site.register(Content, ContentAdmin)
admin.site.register(AudioContent, AudioContentAdmin)
admin.site.register(VideoContent, VideoContentAdmin)