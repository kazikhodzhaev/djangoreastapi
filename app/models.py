from django.db import models
# Create your models here.


# класс страницы 
class Page(models.Model):
	title = models.CharField(max_length=100)
	date_created = models.DateTimeField(auto_now_add=True)


# класс для текстового контента которое подключается к классу Page 
class Content(models.Model):
	page = models.ForeignKey(Page, related_name='text_content', on_delete=models.CASCADE)
	title = models.CharField(max_length=100)
	text = models.TextField()
	date_created = models.DateTimeField(auto_now_add=True)
	count = models.IntegerField()

	class Meta:
		ordering = ['date_created']

	def __str__(self):
		return self.title


# класс для аудио контента которое подключается к классу Page
class AudioContent(models.Model):
	page = models.ForeignKey(Page, related_name='audio_content', on_delete=models.CASCADE)
	title = models.CharField(max_length=100)
	file = models.FileField(upload_to='audio')
	date_created = models.DateTimeField(auto_now_add=True)
	count = models.IntegerField()

	class Meta:
		ordering = ['date_created']

	def __str__(self):
		return self.title


# класс для видео контента которое подключается к классу Page
class VideoContent(models.Model):
	page = models.ForeignKey(Page, related_name='video_content', on_delete=models.CASCADE)
	title = models.CharField(max_length=100)
	file = models.FileField(upload_to='video')
	date_created = models.DateTimeField(auto_now_add=True)
	count = models.IntegerField()

	class Meta:
		ordering = ['date_created']

	def __str__(self):
		return self.title

